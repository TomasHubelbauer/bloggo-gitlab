# GitLab

- [ ] Check out GitLab as a GitHub CI provider: https://about.gitlab.com/features/github/

## Transfer a repository between users

https://gitlab.com/gitlab-org/gitlab-ce/issues/18423

1. Create a dummy group for which both users are owners
2. Have the old user transfer ownership of the project from themselves to the group
3. Have the new user transfer ownership from the group to themselves
4. Delete the dummy group

## Convert a user to a group

**Currently this seems to be broken as the user names do not get released!**

https://gitlab.com/gitlab-org/gitlab-ce/issues/25379
